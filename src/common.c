/* common functions for dgutils */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "./common.h"

extern int cmn_getline(FILE *descr, char **line)
{
	return cmn_getline_(descr, &line, 80 * sizeof(char), 20 * sizeof(char));
}

extern int cmn_getline_(FILE *descr, char ***line,
		size_t base_size, size_t grow)
{
	int c;
	int index = 0;
	int size = (int) base_size;
	**line = (char *) malloc(base_size);
	if (**line == NULL)
		return -2;
	memset(**line, (int)'\0', base_size);
CMN_GETLINE_LOOP:
	c = getc(descr);
	if (c == EOF) {
		(**line)[index] = '\0';
		return -1;
	} else if (c == (int)'\n') {
		(**line)[index] = '\0';
		return index;
	} else if (index == (size - 1)) {
		**line = realloc(**line, (size_t) size + grow);
		if (**line == NULL)
			return -2;
		size += (int) grow;
	}
	(**line)[index++] = (char) c;
	goto CMN_GETLINE_LOOP;
}
