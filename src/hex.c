/* hexdump */

#include <stdio.h>
#include <string.h>

int main(int argc, char **argv)
{
	int b0 = 0, b1 = 0, bytes = 0;
	while ((b0 = getc(stdin)) != EOF) {
		if (bytes == 16) {
			printf("\n");
			bytes = 0;
		} else if (bytes == 8) {
			printf(" ");
		}
		bytes++;
		if (bytes % 2 == 1) {
			b1 = b0;
		} else {
			printf("%2.2x%2.2x ", (unsigned int)b0, (unsigned int)b1);
		}
	}
	if (bytes % 2 == 1) {
		printf("00%2.2x ", (unsigned int)b1);
	}
	printf("\n");
	return 0;
}
