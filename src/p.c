/* pager */

#include <stdio.h>
#include <unistd.h>
#include <fcntl.h>
#include <sys/stat.h>

#define BREAK 10

int main(void)
{
	int ch, counter = 1, console;
	if ((console = open("/dev/tty", O_RDONLY)) < 0) {
		perror(NULL);
		return 1;
	}
	while ((ch = getchar()) != EOF) {
		if (ch != '\n' || counter++ % BREAK != 0) {
			putchar(ch);
			continue;
		}
		printf("\n--[%d] ", counter - 1);
		fflush(stdout);
		if ((read(console, &ch, 1)) == -1) {
			perror(NULL);
			return 1;
		}
		if (ch == 'q') {
			read(console, &ch, 1);
			return 0;
		}
	}
	return 0;
}
