/* prints numbers of lines longer than 80 characters */

#include <stdio.h>
#include <string.h>
#define MAXLENGTH 80

int main(int argc, char **argv)
{
	int c;
	long linelength = 0, linenumber = 1;
	while ((c = getc(stdin)) != EOF) {
		if (c != '\n') {
			if (c == '\t') {
				linelength += 8;
			} else {
				linelength++;
			}
		} else {
			if (linelength > MAXLENGTH)
				printf("%li\n", linenumber);
			linenumber++;
			linelength = 0;
		}
	}
	return 0;
}
