/* list content of directory */

#include <stdio.h>
#include <stdlib.h>
#include <dirent.h>

int main(int argc, char **argv)
{
	char *path;
	DIR *dirp;
	struct dirent *dp;
	if (argc > 2) {
		fputs("Usage: ls [<path>]\n", stderr);
		return 1;
	} else if (argc == 2) {
		path = *++argv;
	} else {
		path = ".";
	}
	if ((dirp = opendir(path)) == NULL) {
		perror("ls");
		return 1;
	}
	while ((dp = readdir(dirp)) != NULL) {
		if ((dp->d_name[0] == '.' && dp->d_name[1] == '\0')
			|| (dp->d_name[0] == '.'
				&& dp->d_name[1] == '.'
				&& dp->d_name[2] == '\0'))
			continue;
		printf("%s\n", dp->d_name);
	}
	closedir(dirp);
	return 0;
}
