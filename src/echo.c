/* echoes all arguments to stdout */

#include <stdio.h>

int main(int argc, char **argv)
{
	while (argc-->1)
		printf("%s ", *++argv);
	putchar('\n');
	return 0;
}
