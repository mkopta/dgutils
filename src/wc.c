/* counts lines and characters */

#include <stdio.h>
#include <errno.h>
#include <string.h>

void print_stat(long bytes, long lines, char *filename)
{
	printf("%4li %4li", lines, bytes);
	if (strlen(filename) != 0) {
		printf(" %s", filename);
	}
	printf("\n");
}

static void readstdin(void)
{
	long bytes = 0;
	long lines = 0;
	int c;
	while ((c = getc(stdin)) != EOF) {
		bytes++;
		if (c == '\n') lines++;
	}
	print_stat(bytes, lines, "");
}

int main(int argc, char **argv)
{
	FILE *fp = NULL;
	int c;
	int onlyfiles = 0;
	long bytes = 0;
	long lines = 0;
	if (argc == 1) {
		readstdin();
		return 0;
	}
	while (argc-->1) {
		++argv;
		if ((strcmp("--", *argv) == 0) && onlyfiles == 0) {
			onlyfiles = 1;
			continue;
		} else if ((strcmp("-", *argv) == 0) && onlyfiles == 0) {
			readstdin();
			continue;
		}
		fp = fopen(*argv, "r");
		if (fp == NULL) {
			perror("wc: fopen");
			return 1;
		}
		while ((c = getc(fp)) != EOF) {
			bytes++;
			if (c == '\n') lines++;
		}
		(void) fclose(fp);
		fp = NULL;
		print_stat(bytes, lines, *argv);
		bytes = 0L;
		lines = 0L;
	}
	return 0;
}
