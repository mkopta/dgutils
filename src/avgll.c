/* calculates average line length */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "./common.h"

int main(int argc, char **argv)
{
	long unsigned int lines = 0, length_overall = 0;
	char *line = NULL;
	int retval = 0;
	while ((retval = cmn_getline(stdin, &line)) >= 0) {
		length_overall += strlen(line) - 1; /* minus newline */
		lines++;
		free(line);
	}
	if (retval == -2) {
		perror("avgll");
		return 1;
	}
	printf("%.2f\n", ((double)length_overall/(double)lines));
	return 0;
}
