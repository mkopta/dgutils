/* removes all \r characters */

#include <stdio.h>

int main(void)
{
	int c;
	while ((c = getchar()) != EOF)
		if (c != '\r')
			putchar(c);
	return 0;
}
