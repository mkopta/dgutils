/* removes a directory */

#include <stdio.h>
#include <unistd.h>

int main(int argc, char **argv)
{
	if (argc != 2) {
		fputs("Usage: rd <dirname>\n", stderr);
		return 1;
	}
	if (rmdir(*++argv) != 0) {
		perror("rd");
		return 1;
	}
	return 0;
}
