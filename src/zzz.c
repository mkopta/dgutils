/* suspends computer to ram */

#include <stdio.h>

int main(void)
{
	FILE *fp = NULL;
	fp = fopen("/sys/power/state", "w");
	if (fp == NULL) {
		perror("zzz: fopen");
		return 1;
	}
	if (fputs("mem", fp) == EOF) {
		perror("zzz: fputs");
		fclose(fp);
		return 1;
	}
	fclose(fp);
	return 0;
}
