/* print list of logged users */

#include <stdio.h>
#include <utmp.h>
#include <string.h>

int main(void)
{
	FILE *fp;
	int c;
	char *p;
	struct utmp record;
	unsigned int i = 0;
	unsigned int index = 0;
	fp = fopen("/var/run/utmp", "r");
	if (!fp) {
		fprintf(stderr, "Cannot open /var/run/utmp\n");
		return 1;
	}
	memset(&record, 0, sizeof record);
	p = (char *)&record;
	while ((c = getc(fp)) != EOF) {
		p[index] = (char)c;
		i++;
		index = i % sizeof record;
		if (index == 0) {
			if (record.ut_type == USER_PROCESS) {
				printf("%5s\t%10s\n", record.ut_line, record.ut_user);
			}
			memset(&record, 0, sizeof record);
		}
	}
	fclose(fp);
	return 0;
}
