#include <stdio.h>

int main(int argc, char **argv)
{
	if (argc > 2) {
		fputs("Usage: yes [string]\n", stderr);
		return 1;
	} else if (argc == 1) {
		for (;;)
			puts("y");
	} else {
		for (++argv;;)
			puts(*argv);
	}
}
