#ifndef __common_h_
#define __common_h_

/* Get line of input
 * Ending \n isn't included, at end is appended '\0'
 * Returned line is explicitly allocated space -> free()
 * returns
 *        >=0 ok (length of string without trailing '\0')
 *         -1 EOF (for length of string use strlen() )
 *         -2 error (malloc, realloc)
 */
extern int cmn_getline(FILE *descr, char **line);
extern int cmn_getline_(FILE *descr, char ***line,
		size_t base_size, size_t grow);

#endif /* __common_h_ */
