/* netcat daemon */

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <netdb.h>
#include <arpa/inet.h>
#include <netinet/in.h>
#include <sys/socket.h>

#define BUFFSIZE 4096

int main(int argc, char **argv)
{
	int port, sock, c_sock;
	ssize_t msg_length;
	struct sockaddr_in sin, rem_addr;
	unsigned int rem_addr_length;
	char buffer[BUFFSIZE];

	if (argc != 2) {
		fputs("Usage: ncd <port 1-65535>\n", stderr);
		return 1;
	}
	port = atoi(*++argv);
	if (port < 1 || port > 65535) {
		fputs("Port is out of range.\n", stderr);
		return 1;
	}
	sock = socket(AF_INET, SOCK_STREAM, IPPROTO_TCP);
	if (sock == -1) {
		perror("ncd");
		return 1;
	}
	memset(&sin, 0, sizeof(sin));
	sin.sin_family = AF_INET;
	sin.sin_port = htons((uint16_t)port);
	if (bind(sock, (struct sockaddr *)&sin, sizeof(sin)) == -1) {
		perror("ncd");
		close(sock);
		return 1;
	}
	if (listen(sock, 5) == -1) {
		perror("ncd");
		close(sock);
		return 1;
	}
	rem_addr_length = (unsigned int) sizeof(rem_addr);
	c_sock = accept(sock, (struct sockaddr *)&rem_addr, &rem_addr_length);
	if (c_sock == -1) {
		perror("ncd");
		close(sock);
		return 1;
	}
NCD_LOOP:
	memset(&buffer, 0, sizeof(buffer));
	msg_length = recv(c_sock, buffer, sizeof(buffer), 0);
	if (msg_length == -1) {
		perror("ncd");
		close(c_sock);
		close(sock);
		return 1;
	}
	if (msg_length == 0)
		goto NCD_LOOP_END;
	fputs(buffer, stdout);
	goto NCD_LOOP;
NCD_LOOP_END:
	close(c_sock);
	close(sock);
	return 0;
}
