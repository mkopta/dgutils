/* trim whitespace from right */

#include <stdio.h>
#include <stdlib.h>
#include <ctype.h>
#include <string.h>
#include "./common.h"

int main(void)
{
	char *line = NULL;
	char *trimed_line = NULL;
	int retval = 0, i, trimed = 0;
	while ((retval = cmn_getline(stdin, &line)) >= 0) {
		trimed_line = malloc(retval * sizeof(char) + sizeof(char));
		memset(trimed_line, '\0', retval * sizeof(char) + sizeof(char));
		for (i = retval - 1; i >= 0; i--) {
			if ((trimed == 1) || isspace(line[i]) == 0) {
				trimed = 1;
				trimed_line[i] = line[i];
			}
		}
		printf("%s\n", trimed_line);
		free(trimed_line);
		free(line);
		trimed_line = NULL;
		line = NULL;
		trimed = 0;
	}
	if (retval == -1) {
		return 0;
	} else {
		fputs("rtrim: malloc/realloc error\n", stderr);
		return 1;
	}
	return 0;
}
