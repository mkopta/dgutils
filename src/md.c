/* make directory */

#include <stdio.h>
#include <sys/stat.h>

int main(int argc, char **argv)
{
	__mode_t mask = umask(0);
	if (argc < 2) {
		fputs("Usage: md <dirname>\n", stderr);
		return 1;
	}
	++argv;
	if (mkdir(*argv, 0777 - mask) != 0) {
		perror("md");
		return 1;
	}
	return 0;
}
