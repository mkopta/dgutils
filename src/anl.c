/* counts frequency of ascii characters */

#include <stdio.h>
#include <string.h>
#include <stdlib.h>

#define SIZE 127

/* ascii table */
char *symbol[] = {
	"NUL", "SOH", "STX", "ETX", "EOT", "ENQ", "ACK", "BEL", "BS", "HT", "LF",
	"VT", "FF", "CR", "SO", "SI", "DLE", "DC1", "DC2", "DC3", "DC4", "NAK", "SYN",
	"ETB", "CAN", "EM", "SUB", "ESC", "FS", "GS", "RS", "US", "SPACE", "!", "\"",
	"#", "$", "%", "&", "'", "(", ")", "*", "+", ",", "-", ".", "/", "0", "1",
	"2", "3", "4", "5", "6", "7", "8", "9", ":", ";", "<", "=", ">", "?", "@",
	"A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L", "M", "N", "O",
	"P", "Q", "R", "S", "T", "U", "V", "W", "X", "Y", "Z", "[", "\\", "]", "^",
	"_", "`", "a", "b", "c", "d", "e", "f", "g", "h", "i", "j", "k", "l", "m",
	"n", "o", "p", "q", "r", "s", "t", "u", "v", "w", "x", "y", "z", "{", "|",
	"}", "~", "DEL" };

int main(int argc, char **argv)
{
	int i, c;
	long *frequency = calloc(SIZE, sizeof(long));
	while ((c = getc(stdin)) != EOF) {
		if (c >= SIZE) continue;
		frequency[c]++;
	}
	for (i = 0; i < SIZE; i++)
		if (frequency[i] > 0)
			printf("%li %s\n", frequency[i], symbol[i]);
	return 0;
}
