/* writes stdin to a file */

#include <stdio.h>

static int tf(FILE *file)
{
	int c = 0;
	while ((c = getchar()) != EOF) {
		if (putc(c, file) == EOF) {
			perror("tf: putc");
			return 1;
		}
	}
	return 0;
}

int main(int argc, char **argv)
{
	FILE *fp = NULL;
	if (argc != 2) {
		fputs("tf: usage: tf <output file>\n", stderr);
		return 1;
	}
	fp = fopen(*++argv, "w");
	if (fp == NULL) {
		perror("fopen");
		fputs("tf: opening output file failed.\n", stderr);
		return 1;
	}
	if (tf(fp) == 1) {
		fputs("tf: writing into output file failed.\n", stderr);
		(void) fclose(fp);
		return 1;
	}
	if (fclose(fp) == EOF) {
		perror("tf: fclose");
		fputs("tf: closing output file failed.\n", stderr);
		return 1;
	}
	return 0;
}
