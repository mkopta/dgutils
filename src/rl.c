/* prints destination of symbolic link */

#define _GNU_SOURCE
#include <stdio.h>
#include <unistd.h>
#include <string.h>

#define BUFFSIZE 2048

int main(int argc, char **argv)
{
	char buf[BUFFSIZE];
	int bytes;
	if (argc != 2) {
		fputs("Usage: rl <symlink>\n", stderr);
		return 1;
	}
	memset(buf, 0, BUFFSIZE);
	bytes = (int) readlink(*++argv, buf, BUFFSIZE);
	if (bytes == -1) {
		perror("rl");
		return 1;
	} else if (bytes == BUFFSIZE) {
		fprintf(stderr, "rl: too long\n");
		return 1;
	} else {
		printf("%s\n", buf);
	}
	return 0;
}
