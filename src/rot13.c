/* caesar cipher */

#include <stdio.h>
#include <errno.h>
#include <string.h>
#include <ctype.h>

#define ROT 13

int main(int argc, char **argv)
{
	int c = 0;
	while ((c = getc(stdin)) != EOF) {
		if (isalpha(c)) {
			c += ROT;
			if (((c - ROT <= 'Z') && (c > 'Z')) || (c > 'z'))
				c -= 26;
		}
		putc(c, stdout);
	}
	return 0;
}
