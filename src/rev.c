/* reverses character order on each input line */

#include <stdio.h>
#include <stdlib.h>
#include "./common.h"

int main(int argc, char **argv)
{
	char *line;
	int retval = 0, i;

	while ((retval = cmn_getline(stdin, &line)) >= 0) {
		for (i = retval - 1; i >= 0; i--)
			putc(line[i], stdout);
		printf("\n");
		free(line);
		line = NULL;
	}
	if (retval != -1) {
		printf("rev: malloc/realloc error\n");
		return 1;
	}
	return 0;
}
