/* netcat client */

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <netdb.h>
#include <arpa/inet.h>
#include <netinet/in.h>
#include <sys/socket.h>

#define BUFFSIZE 4096

int main(int argc, char **argv)
{
	char *ip = 0, *line = 0;
	int port, sock;
	struct hostent *he;
	struct in_addr inaddr;
	struct sockaddr_in sin;
	size_t line_buf_len = 0;
	ssize_t curr_line_len;

	if (argc != 3) {
		printf("Usage: ncc <host> <port>\n", stderr);
		return 1;
	}
	port = atoi(*(argv+2));
	if (port < 1 || port > 65535) {
		fputs("Port is out of range.\n", stderr);
		return 1;
	}
	he = gethostbyname(*(argv+1));
	if (!he) {
		fputs("nc: cannot resolve host\n", stderr);
		return 1;
	}
	memcpy((char *)&inaddr, *he->h_addr_list, sizeof(inaddr));
	ip = inet_ntoa(inaddr);
	sock = socket(AF_INET, SOCK_STREAM, IPPROTO_TCP);
	if (sock == -1) {
		perror("ncc");
		return 1;
	}
	memset(&sin, 0, sizeof(sin));
	sin.sin_family = AF_INET;
	sin.sin_port = htons((uint16_t)port);
	if (inet_aton(ip, &sin.sin_addr) == 0) {
		perror("ncc");
		close(sock);
		return 1;
	}
	if (connect(sock, (struct sockaddr*)&sin, sizeof(sin)) == -1) {
		perror("ncc");
		close(sock);
		return 1;
	}
	while ((curr_line_len = getline(&line, &line_buf_len, stdin)) > 0)
		write(sock, line, (size_t)curr_line_len);
	close(sock);
	return 0;
}
