/* removes a file */

#include <stdio.h>
#include <unistd.h>

int main(int argc, char **argv)
{
	if (argc != 2) {
		fputs("Usage: rm <filename>\n", stderr);
		return 1;
	}
	if (remove(*++argv) != 0) {
		perror("rm");
		return 1;
	}
	return 0;
}
