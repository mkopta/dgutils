/* prints first N lines */

#include <stdio.h>
#include <stdlib.h>


int main(int argc, char **argv)
{
	int lines, c, count = 0;
	++argv;
	if (argc == 1) {
		lines = 10;
	} else if (argc == 3
		&& (**argv == '-'
			&& *(*argv+1) == 'n'
			&& *(*argv+2) == '\0')) {
		++argv;
		lines = atoi(*argv);
		if (lines == 0)
			return 0;
	} else {
		fputs("Usage: head [-n <lines>]\n", stderr);
		return 1;
	}
	while ((c = getchar()) != EOF) {
		putchar(c);
		if (c == '\n')
			if (++count == lines)
				break;
	}
	return 0;
}
