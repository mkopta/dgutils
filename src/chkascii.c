/* checks whether given input is ascii only */

#include <stdio.h>

int main(void)
{
	int c;
	while ((c = getchar()) != EOF)
		if (c > 127)
			return 1;
	return 0;
}
