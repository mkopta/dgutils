/* reads content of arguments to stdout */

#include <stdio.h>
#include <errno.h>

int main(int argc, char **argv)
{
	int c = 0;
	FILE *fp = NULL;
	if (argc == 1) {
		while ((c = getchar()) != EOF)
			putchar(c);
		return 0;
	}
	while (argc-->1) {
		fp = fopen(*++argv, "r");
		if (fp == NULL) {
			perror("cat");
			return 1;
		}
		while ((c = getc(fp)) != EOF)
			putchar(c);
		fclose(fp);
	}
	return 0;
}
