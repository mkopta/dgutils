/* checks for \r characters */

#include <stdio.h>

int main(void)
{
	int c;
	while ((c = getchar()) != EOF)
		if (c == '\r')
			return 1;
	return 0;
}
