/* find lines with nonascii chars */

#include <stdio.h>

int main(int argc, char **argv)
{
        int c, lines = 1;
        while ((c = getchar()) != EOF) {
                if (c > 127)
                                printf("%i\n", lines);
                if (c == '\n')
                                lines++;
        }
        return 0;
}
