/* compares two files */

#include <stdio.h>

int main(int argc, char **argv)
{
	FILE *f1, *f2;
	int b1, b2, differs = 0;
	if (argc != 3) {
		fputs("Usage: cmp <file1> <file2>\n", stderr);
		return 1;
	}
	f1 = fopen(*++argv, "r");
	f2 = fopen(*++argv, "r");
	if (f1 == NULL || f2 == NULL) {
		perror("cmp");
		if (f1 != NULL) fclose(f1);
		if (f2 != NULL) fclose(f2);
		return 1;
	}
CMP_LOOP:
	b1 = getc(f1);
	b2 = getc(f2);
	if (b1 == b2) {
		if (b1 == EOF)
			goto CMP_LOOP_EQ_END;
		goto CMP_LOOP;
	}
	differs = 1;
CMP_LOOP_EQ_END:
	fclose(f1);
	fclose(f2);
	return differs;
}
