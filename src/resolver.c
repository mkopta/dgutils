/* resolve domain to IP */

#include <stdio.h>
#include <stdlib.h>
#include <netdb.h>
#include <sys/socket.h>
#include <arpa/inet.h>
#include <netinet/in.h>

int main(int argc, char **argv)
{
	unsigned int i = 0;
	struct hostent *he;
	struct in_addr *in;
	char *ip = NULL;
	if (argc != 2) {
		fputs("Usage: resolver <domain>\n", stderr);
		return 1;
	}
	he = gethostbyname(*++argv);
	if (!he) {
		fprintf(stderr, "Cannot resolve \"%s\"\n", *argv);
		return 1;
	}
	while (he->h_addr_list[i] != NULL) {
		in = (struct in_addr*) he->h_addr_list[i];
		ip = inet_ntoa(*in);
		printf( "%s\n", ip);
		i++;
	}
	return 0;
}
