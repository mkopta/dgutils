/* create (sym)link */

#include <stdio.h>
#include <unistd.h>

int main(int argc, char *argv[])
{
	if (argc == 3) {
		if (link(argv[1], argv[2]) != 0) {
			perror("ln");
			return 1;
		}
	} else if (argc == 4
		&& ((argv[1][0] == '-'
			&& argv[1][1] == 's'
			&& argv[1][2] == '\0'))) {
		if (symlink(argv[2], argv[3]) != 0) {
			perror("ln");
			return 1;
		}
	} else {
		fputs("Usage: ln [-s] <target> <link_name>\n", stderr);
		return 1;
	}
	return 0;
}
