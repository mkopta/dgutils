/* makes pipe */

#include <stdio.h>
#include <sys/stat.h>

int main(int argc, char **argv)
{
	__mode_t mask = umask(0);
	if (argc < 2) {
		fputs("Usage: mp <fifo>\n", stderr);
		return 1;
	}
	++argv;
	if (mkfifo(*argv, 0777 - mask) != 0) {
		printf("mf: %s: ", *argv);
		(void) fflush(stdout);
		perror("");
		return 1;
	}
	return 0;
}
