/* reads numbers from stdin and performs given operation on them */

#include <stdio.h>
#include <string.h>

int main(int argc, char **argv)
{
	char *op = NULL;
	float f, acc = 0;
	if (argc > 2) {
		fprintf(stderr, "Usage: %s [add|sub|mul]\n", *argv);
		return 1;
	} else if (argc == 2) {
		op = *(argv + 1);
	} else {
		op = "add";
	}
	if (strcmp(op, "add") == 0) {
		while (scanf("%f", &f) != EOF)
			acc += f;
	} else if (strcmp(op, "sub") == 0) {
		while (scanf("%f", &f) != EOF)
			acc -= f;
	} else if (strcmp(op, "mul") == 0) {
		acc = 1;
		while (scanf("%f", &f) != EOF)
			acc *= f;
	} else {
		fprintf(stderr, "Invalid operation '%s'.\n", op);
		return 1;
	}
	printf("%f\n", acc);
	return 0;
}
