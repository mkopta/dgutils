/* count lines */

#include <stdio.h>

int main(void)
{
	int c;
	unsigned long lines = 0;
	while ((c = getc(stdin)) != EOF)
		if (c == '\n') lines++;
	printf("%lu\n", lines);
	return 0;
}
